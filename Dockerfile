## MAINTAINER   Shambo Bose <shambo.bose@aol.com>
FROM composer:1.6.5 as buildenv 
WORKDIR /stage
COPY ./app /stage
RUN composer update &&  mv .env.example .env && php artisan key:generate

FROM php:7.2.30-apache
EXPOSE 80
COPY --from=buildenv /stage /www
COPY vhost.conf /etc/apache2/sites-available/000-default.conf
RUN chown -R www-data:www-data /www \
    && a2enmod rewrite
